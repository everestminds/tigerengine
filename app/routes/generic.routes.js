module.exports = app => {
    const generic = require("../controllers/generic.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    // router.post("/", generic.create);

    router.post("/:model", generic.create);
  
    // Retrieve all Tutorials
    router.get("/:model", generic.findAll);
  
    // Retrieve all published Tutorials
    router.get("/published", generic.findAllPublished);
  
    // Retrieve a single Tutorial with id
    router.get("/:model/:id", generic.findOne);
  
    // Update a Tutorial with id
    router.put("/:model/:id", generic.update);
  
    // Delete a Tutorial with id
    router.delete("/:model/:id", generic.delete);
  
    // Create a new Tutorial
    router.delete("/", generic.deleteAll);
  
    app.use('/api', router);
  };