module.exports = (sequelize, Sequelize) => {
    const Form = sequelize.define("form", {
        status: {
            type:Sequelize.ENUM(['pending','rejected','approved']),
            defaultValue:'pending',
            allowNull:false
          },
    });
    return Form;
  };
  