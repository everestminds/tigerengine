const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;




db.roles = require("./role.model")(sequelize,Sequelize);
db.users = require("./user.model")(sequelize,Sequelize);
db.steps = require("./step.model.js")(sequelize,Sequelize);
db.forms = require("./form.models.js")(sequelize,Sequelize);
db.policies = require("./policy.models.js")(sequelize,Sequelize);
db.tasks = require("./task.model.js")(sequelize,Sequelize);
db.transitions = require("./transition.model.js")(sequelize,Sequelize);


require('./associations.js')(db);



module.exports = db;