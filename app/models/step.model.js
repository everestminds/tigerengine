module.exports = (sequelize, Sequelize) => {
  const Step = sequelize.define("step", {
    type: {
        type:Sequelize.ENUM(['parallel','sequential']),
        defaultValue:'sequential'
    },
    status: {
      type:Sequelize.ENUM(['pending','rejected','completed']),
      defaultValue:'pending',
      allowNull:false
    },
  });

  return Step;
};
