module.exports = (sequelize, Sequelize) => {
    const Task = sequelize.define("task", {
        status: {
            type:Sequelize.ENUM(['pending','rejected','completed']),
            defaultValue:'pending',
            allowNull:false
          },
    });
    return Task;
  };
  